#  http://docs.tweepy.org/en/latest/index.html

import logging

try:
    import tweepy
except ImportError:
    logging.error('Ne peux pas importer tweepy')

consumer_key = 'abc'
consumer_secret = 'abc'
access_token = 'abc'
access_token_secret = 'abc'

tweet = 'Hello World! #helloworld'
picture = '/path/to/picture.png'

try:
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)

    api = tweepy.API(auth)

    api.update_with_media(filename=picture, status=tweet)

    logging.info('à tweeté: %s' % tweet)
except Exception:
    logging.exception('Erreur lors du tweet')
